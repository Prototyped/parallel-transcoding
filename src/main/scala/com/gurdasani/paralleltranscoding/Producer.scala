package com.gurdasani.paralleltranscoding

import java.io.File
import java.util.function.Consumer

import com.amazonaws.services.sqs.model.SendMessageRequest

class Producer(sqsConfigurer: SQSConfigurer) extends Consumer[File] {
  override def accept(file: File): Unit = {
    val message = new SendMessageRequest()
        .withQueueUrl(sqsConfigurer.sqsUrl)
        .withMessageBody(file.getPath)
    sqsConfigurer.client.sendMessage(message)
  }
}
