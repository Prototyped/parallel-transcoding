package com.gurdasani.paralleltranscoding

import com.amazonaws.services.sqs.model.ReceiveMessageRequest
import com.google.common.collect.ImmutableList
import com.google.common.util.concurrent.RateLimiter
import com.gurdasani.paralleltranscoding.ConsumerMain.HardwareTranscode
import com.gurdasani.paralleltranscoding.HostType.Host
import org.apache.commons.exec.{CommandLine, DefaultExecutor, PumpStreamHandler}
import org.kohsuke.args4j.{CmdLineParser, Option}
import org.slf4j.LoggerFactory

import java.io.{ByteArrayOutputStream, File}
import java.nio.charset.StandardCharsets
import java.util.concurrent._

object ConsumerMain {
  private[this] val logger = LoggerFactory.getLogger(classOf[String])
  def main(args: Array[String]): Unit = {
    val argBean = new ConsumerArgs
    val cmdLineParser = new CmdLineParser(argBean)
    cmdLineParser.parseArgument(ImmutableList.copyOf(args))
    argBean.validate()
    val host = Host(argBean.num, argBean.prefix)

    val queueName = "parallel-transcode"
    val sqs = new SQSConfigurer(queueName, argBean.profile)
    def listToOption[T](list: java.util.List[T]): scala.Option[T] = {
      if (list.isEmpty)
        None
      else {
        val iterator = list.iterator()
        val first = iterator.next()
        require(!iterator.hasNext, "list must have no more than one element")
        Some(first)
      }
    }

    val receiveRequest = new ReceiveMessageRequest(sqs.sqsUrl)
      .withWaitTimeSeconds(20)
      .withVisibilityTimeout(600)
      .withMaxNumberOfMessages(1)

    val transcoder = new Transcoder(argBean.ffmpeg, argBean.ffprobe, Encoding(argBean.encoding),
      HardwareTranscode(argBean.hw), host, sqs, argBean.avx512,
      scala.Option(argBean.heterogeneousCoreType).map(HeterogeneousCoreType(_)),
      scala.Option(argBean.logicalProcessorOrdinal).map(_.toInt))

    val executor = new ThreadPoolExecutor(argBean.concurrency, argBean.concurrency, 1, TimeUnit.HOURS,
                       new SynchronousQueue[Runnable])
    val rejectedExecutionHandler = new RejectedExecutionHandler {
      override def rejectedExecution(runnable: Runnable, threadPoolExecutor: ThreadPoolExecutor): Unit = {
        threadPoolExecutor.getQueue.put(runnable)
      }
    }
    executor.setRejectedExecutionHandler(rejectedExecutionHandler)

    val rateLimiter = RateLimiter.create(0.05)

    def transcodeForMessages = {
      def task: Runnable = () => {
        for (message <- listToOption(sqs.client.receiveMessage(receiveRequest).getMessages)) {
          try {
            transcoder.transcode(message)
          } catch {
            case e: Exception =>
              logger.error("Transcode failed, not deleting message {}: {}",
                message.getReceiptHandle, e.getMessage, e)
          } finally {
            rateLimiter.acquire()
          }
        }
      }

      task
    }

    LazyList.continually(elem = transcodeForMessages).foreach { task =>
      executor submit task
    }
  }

  sealed trait Encoding {
    val suffix: String
    def videoEncodingArgs(fullLoc: String, bits: Int, hw: HardwareTranscode, avx512: Boolean): Array[Array[String]]
  }
  case object WebM extends Encoding {
    override val suffix = ".webm";

    override def videoEncodingArgs(fullLoc: String, bits: Int, hw: HardwareTranscode, avx512: Boolean): Array[Array[String]] = {
      val twoPassFile = fullLoc + ".2pass"
      Array(
        Array("-pass", "1", "-passlogfile", twoPassFile, "-speed", "4", "-b:v", "0", "-threads",
          "16", "-c:v", "libvpx-vp9", "-tile-columns", "6", "-frame-parallel", "1", "-crf", "30", "-row-mt", "1",
          "-f", "webm"),
        Array("-pass", "2", "-passlogfile", twoPassFile, "-speed", "1", "-b:v", "0", "-threads",
          "16", "-c:v", "libvpx-vp9", "-tile-columns", "6", "-frame-parallel", "1", "-crf", "30", "-row-mt", "1",
          "-f", "webm"))
    }
  }

  case object HEVC extends Encoding {
    override val suffix = ".mkv";

    override def videoEncodingArgs(fullLoc: String, bits: Int, hw: HardwareTranscode, avx512: Boolean): Array[Array[String]] = {
      val pixelFormat = bits match {
        case 8 => "yuv420p"
        case 10 => "yuv420p10le"
        case _ => throw new IllegalArgumentException(s"Unsupported bitness $bits")
      }

      val profile = bits match {
        case 8 => "main"
        case 10 => "main10"
        case _ => throw new IllegalArgumentException(s"Unsupported bitness $bits")
      }

      val x265StandardParams = "lookahead-slices=5:no-temporal-layers=1:keyint=250:pbratio=1.30:no-lossless=1:ipratio=1.40:deblock=0_0:cplxblur=20.0:qpstep=4:crqpoffs=0:b-pyramid=1:temporal-mvp=1:max-tu-size=32:qpmax=51:interlace=0:min-keyint=23:wpp=1:psy-rdoq=0.00:rdoq-level=0:strong-intra-smoothing=1:cbqpoffs=0:qblur=0.5:open-gop=1:qpmin=0:no-cu-lossless=1:rc=2:qcomp=0.60:no-constrained-intra=1:qg-size=32:no-tskip=1:no-tskip-fast=1:bframe-bias=0:rdpenalty=0:no-sao-non-deblock=1:no-intra-refresh=1:aq-strength=1.00:psy-rd=2.00:no-rd-refine=1"
      val x265Params = if (avx512) x265StandardParams + ":asm=avx512" else x265StandardParams
      Array(
        Array("-c:v", "libx265", "-preset", "slow", "-crf", "23", "-profile:v", profile,
          "-pix_fmt", pixelFormat, "-x265-params", x265Params))
    }
  }

  case object HardwareHEVC extends Encoding {
    override val suffix = ".mkv"

    override def videoEncodingArgs(fullLoc: String, bits: Int, hw: HardwareTranscode, avx512: Boolean): Array[Array[String]] = {
      Array(
        hw.hardwareEncodeArgs
      )
    }
  }

  object Encoding {
    def apply(arg: String): Encoding = arg.toLowerCase match {
      case "webm" => WebM
      case "hevc" => HEVC
      case "hw" => HardwareHEVC
      case _ => throw new IllegalArgumentException(s"encoding must be webm, hevc or hw. Invalid: $arg")
    }
  }

  sealed trait HardwareTranscode {
    def hardwareDecodeArgs(mediaEncoding: String): Array[String]
    val hardwareEncodeArgs: Array[String]
    def scaleArgs(width: Int, height: Int): Array[String]
  }

  case object QuickSync extends HardwareTranscode {
    override def hardwareDecodeArgs(mediaEncoding: String): Array[String] = {
      val inputCodec = mediaEncoding match {
        case "av1" => "av1_qsv"
        case "h264" => "h264_qsv"
        case "hevc" => "hevc_qsv"
        case "mjpeg" => "mjpeg_qsv"
        case "mpeg2video" => "mpeg2_qsv"
        case "vc1" => "vc1_qsv"
        case "vp8" => "vp8_qsv"
        case "vp9" => "vp9_qsv"
        case _ => throw new IllegalArgumentException(
          s"QuickSync cannot perform hardware decoding for encoding $mediaEncoding")
      }
      qsvDevice ++ Array("-hwaccel", "qsv", "-hwaccel_output_format", "qsv", "-c:v", inputCodec)
    }

    override val hardwareEncodeArgs: Array[String] = Array("-c:v", "hevc_qsv", "-global_quality:v", "22",
      "-preset", "veryslow", "-rdo", "1", "-adaptive_i", "1", "-adaptive_b", "1", "-extbrc", "1",
      "-look_ahead_depth", "40", "-g", "256", "-b_strategy", "1", "-bf", "7", "-refs", "5")

    override def scaleArgs(width: Int, height: Int): Array[String] = Array(s"vpp_qsv=w=$width:h=$height")

    val qsvDevice: Array[String] = {
      if (!scala.util.Properties.isLinux) Array()
      else {
        Array("-qsv_device") ++ new File("/dev/dri")
            .listFiles(_.getName.startsWith("renderD"))
            .filter(isIntel)
            .take(1)
            .map(_.toString)
      }
    }

    def isIntel(file: File): Boolean = {
      val exec = new DefaultExecutor
      val out = new ByteArrayOutputStream()
      val commandLine = new CommandLine("/usr/bin/vainfo")
      commandLine.addArguments(Array("--display", "drm", "--device", file.getAbsolutePath))
      exec.setStreamHandler(new PumpStreamHandler(out))
      exec.execute(commandLine)
      out.toString(StandardCharsets.UTF_8)
        .lines()
        .anyMatch(
          _.startsWith("vainfo: Driver version: Intel iHD driver"))
    }
  }

  case object VideoToolbox extends HardwareTranscode {
    override def hardwareDecodeArgs(mediaEncoding: String): Array[String] = {
      val inputCodec = mediaEncoding match {
        case "h264" => "h264_videotoolbox"
        case "hevc" => "hevc_videotoolbox"
        case _ => throw new IllegalArgumentException(
          s"VideoToolbox cannot perform hardware decoding for encoding $mediaEncoding")
      }
      Array("-hwaccel", "videotoolbox", "-c:v", inputCodec)
    }

    override val hardwareEncodeArgs: Array[String] = Array("-c:v", "hevc_videotoolbox")

    override def scaleArgs(width: Int, height: Int): Array[String] = Software.scaleArgs(width, height)
  }

  case object CUDA extends HardwareTranscode {
    override def hardwareDecodeArgs(mediaEncoding: String): Array[String] = {
      val inputCodec = mediaEncoding match {
        case "av1" => "av1_cuvid"
        case "h264" => "h264_cuvid"
        case "hevc" => "hevc_cuvid"
        case "mjpeg" => "mjpeg_cuvid"
        case "mpeg1video" => "mpeg1_cuvid"
        case "mpeg2video" => "mpeg2_cuvid"
        case "mpeg4" => "mpeg4_cuvid"
        case "vc1" => "vc1_cuvid"
        case "vp8" => "vp8_cuvid"
        case "vp9" => "vp9_cuvid"
        case _ => throw new IllegalArgumentException(
          s"NVDEC cannot perform hardware decoding for encoding $mediaEncoding")
      }
      Array("-hwaccel", "cuda", "-c:v", inputCodec)
    }

    override val hardwareEncodeArgs: Array[String] = Array("-c:v", "hevc_nvenc")

    override def scaleArgs(width: Int, height: Int): Array[String]
      = Array(s"cudascale=w=$width:h=$height:interp_algo=4")
  }

  case object Software extends HardwareTranscode {
    override def hardwareDecodeArgs(mediaEncoding: String): Array[String] = Array()

    override val hardwareEncodeArgs: Array[String] = Array()

    override def scaleArgs(width: Int, height: Int): Array[String] = Array(s"scale=w=$width:h=$height")
  }

  object HardwareTranscode {
    def apply(arg: String): HardwareTranscode = arg match {
      case "quicksync" => QuickSync
      case "videotoolbox" => VideoToolbox
      case "cuda" => CUDA
      case "none" => Software
      case _ => throw new IllegalArgumentException(s"hw must be none, quicksync, videotoolbox or cuda. Invalid: $arg")
    }
  }
}

class ConsumerArgs {
  @Option(required = false, name = "-c", aliases = Array("--concurrency"), usage = "--concurrency <transcoders-to-start>")
  var concurrency = 1

  @Option(required = true, name = "-a", aliases = Array("--profile"), usage = "--profile <aws-profile-in-config>")
  var profile: String = _

  @Option(required = true, name = "-p", aliases = Array("--path"), usage = "--path /path/to/files")
  var prefix: File = _

  @Option(required = false, name = "-h", aliases = Array("--hw"), usage = "--hw none|quicksync|videotoolbox|cuda")
  var hw: String = "none"

  @Option(required = false, name = "-e", aliases = Array("--encoding"), usage = "--encoding webm|hevc|hw")
  var encoding = "hevc"

  @Option(required = true, name = "-i", aliases = Array("--id"), usage = "--id <integer-host-id>")
  var num = 0

  @Option(required = false, name = "-f", aliases = Array("--ffmpeg"), usage = "--ffmpeg </path/to/ffmpeg>")
  var ffmpeg: File = new File("ffmpeg")

  @Option(required = false, name = "-r", aliases = Array("--ffprobe"), usage = "--ffprobe </path/to/ffprobe>")
  var ffprobe: File = new File("ffprobe")

  @Option(required = false, name = "-5", aliases = Array("--avx512"), usage = "--avx512")
  var avx512: Boolean = false

  @Option(required = false, name = "-t", aliases = Array("--core-type"), usage = "--core-type performance|efficiency")
  var heterogeneousCoreType: String = _

  @Option(required = false, name = "-l", aliases = Array("--logical-processor"),
    usage = "--logical-processor <ordinal>")
  var logicalProcessorOrdinal: java.lang.Integer = _

  def validate(): Unit = {
    import ConsumerMain.Encoding
    require(concurrency > 0, s"concurrency must be a positive integer. Invalid: $concurrency")
    require(num > 0, s"id must be a positive integer. Invalid: $num")
    require(ffmpeg.canExecute, s"ffmpeg must be an executable file. Invalid: $ffmpeg")
    require(prefix.isDirectory, s"path must be a directory. Invalid: $prefix")
    require(!avx512 || encoding == "hevc", "AVX512 only works with HEVC encoding.")

    Encoding(encoding)
    HardwareTranscode(hw)
    scala.Option(heterogeneousCoreType).foreach(HeterogeneousCoreType(_))
    scala.Option(logicalProcessorOrdinal).foreach { id =>
      require(id >= 0, s"logical processor ordinal must be non-negative. Invalid: $id")
    }
  }
}
