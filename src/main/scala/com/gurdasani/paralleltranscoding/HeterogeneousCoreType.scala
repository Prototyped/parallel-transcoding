package com.gurdasani.paralleltranscoding

sealed abstract class HeterogeneousCoreType
case object PerformanceCore extends HeterogeneousCoreType
case object EfficiencyCore extends HeterogeneousCoreType

object HeterogeneousCoreType {
  def apply(str: String): HeterogeneousCoreType = str match {
    case "performance" => PerformanceCore
    case "efficiency" => EfficiencyCore
    case _ => throw new IllegalArgumentException("Acceptable heterogeneous core types are performance and efficiency.")
  }
}