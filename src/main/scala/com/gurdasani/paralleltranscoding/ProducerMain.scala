package com.gurdasani.paralleltranscoding

import java.io.File
import scala.compat.java8.FunctionConverters._

import com.google.common.collect.ImmutableList
import org.kohsuke.args4j.{CmdLineParser, Option}

object ProducerMain {
  def main(args: Array[String]): Unit = {
    val argBean = new ProducerArgs
    val cmdLineParser = new CmdLineParser(argBean)
    cmdLineParser.parseArgument(ImmutableList.copyOf(args))
    argBean.validate()

    val queueName = "parallel-transcode"
    val sqsConfigurer = new SQSConfigurer(queueName, argBean.profile)
    val producer = new Producer(sqsConfigurer)

    if (argBean.stdin) {
      Console.in.lines().map[File](((line: String) => new File(line.trim())).asJava).forEach(producer)
    } else {
      argBean.files.forEach(producer)
    }
  }
}

class ProducerArgs {
  @Option(required = false, name = "-s", aliases = Array("--stdin"), usage = "--stdin")
  var stdin: Boolean = false

  @Option(required = false, name = "-f", aliases = Array("--file"), usage = "--file <relative-source-filename>")
  var files: java.util.List[File] = _

  @Option(required = true, name = "-a", aliases = Array("--profile"), usage = "--profile <aws-profile-in-config>")
  var profile: String = _

  def validate(): Unit = {
    if ((files == null || files.isEmpty) && !stdin) {
      throw new IllegalArgumentException("At least one file must be specified with --file, or --stdin must be used.")
    }
  }
}
