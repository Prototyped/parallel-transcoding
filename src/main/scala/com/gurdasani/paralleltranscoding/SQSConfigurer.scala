package com.gurdasani.paralleltranscoding

import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.auth.{AWSCredentialsProvider, STSAssumeRoleSessionCredentialsProvider}
import com.amazonaws.regions.Regions
import com.amazonaws.services.sqs.{AmazonSQS, AmazonSQSClient}

class SQSConfigurer(queueName: String, profile: String) {
  val credentialsProvider: AWSCredentialsProvider = new ProfileCredentialsProvider(profile)
  val client: AmazonSQS = AmazonSQSClient.builder()
    .withCredentials(credentialsProvider)
    .withRegion(Regions.EU_WEST_1)
    .build()
  val sqsUrl: String = client.createQueue(queueName).getQueueUrl
}
