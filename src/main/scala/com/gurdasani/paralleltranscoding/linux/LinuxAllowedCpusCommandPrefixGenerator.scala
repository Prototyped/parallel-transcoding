package com.gurdasani.paralleltranscoding.linux

import com.gurdasani.paralleltranscoding.{
  CommandPrefixGenerator, EfficiencyCore, HeterogeneousCoreType, PerformanceCore}
import org.slf4j.LoggerFactory

import scala.sys.process._

class LinuxAllowedCpusCommandPrefixGenerator(heterogeneousCoreType: Option[HeterogeneousCoreType],
                                             logicalProcessorOrdinal: Option[Int])
  extends CommandPrefixGenerator {
  require(scala.util.Properties.isLinux, "Restriction to heterogeneous core sets is only supported on Linux.")

  private val logger = LoggerFactory.getLogger(classOf[LinuxAllowedCpusCommandPrefixGenerator])
  private val heterogeneousCorePartitions = LinuxHeterogeneousCorePartitioner.partitionCores()
  private val logicalProcessorPartitions = LinuxLogicalProcessorPartitioner.partitionLogicalProcessors()
  override def generateCommandPrefix(): Seq[String] = {
    val heterogeneousCoreIds = filterHeterogeneousCoreIds()
    val logicalProcessorOrdinals = filterLogicalProcessorOrdinals(heterogeneousCoreIds)
    constructCommand(logicalProcessorOrdinals.sorted)
  }

  private def constructCommand(coreIds: Seq[Int]): Seq[String] = {
    try {
      Seq(
        "/bin/sudo", "systemd-run", "--uid", "id -u".!!.trim, "--gid", "id -g".!!.trim, "--scope", "-p",
        s"""AllowedCPUs=${coreIds.map(_.toString).reduce(_ + "," + _)}""", "--")
    } catch {
      case e: Exception =>
        logger.error("Failed to construct command: {}", e.getMessage, e)
        throw e
    }
  }
  private def filterHeterogeneousCoreIds(): Seq[Int] = heterogeneousCoreType match {
    case Some(PerformanceCore) => heterogeneousCorePartitions.performanceCores
    case Some(EfficiencyCore) => heterogeneousCorePartitions.efficiencyCores
    case Some(_) => throw new IllegalArgumentException(s"Unknown heterogeneous core type $heterogeneousCoreType")
    case None => heterogeneousCorePartitions.performanceCores ++ heterogeneousCorePartitions.efficiencyCores
  }

  private def filterLogicalProcessorOrdinals(coreIds: Seq[Int]): Seq[Int] = logicalProcessorOrdinal match {
    case None => coreIds
    case Some(ord) =>
      val coreIdSet = coreIds.toSet
      logger.info("Core IDs to filter: {}", coreIds)
      logger.info("Logical processor partitions: {}", logicalProcessorPartitions)
      logicalProcessorPartitions.filter { partition =>
        partition.ids.length > ord
      }.map { partition =>
        val ids = partition.ids.toArray
        ids(ord)
      }.filter { id =>
        coreIdSet.contains(id)
      }
  }
}
