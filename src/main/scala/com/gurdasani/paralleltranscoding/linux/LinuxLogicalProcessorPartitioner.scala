package com.gurdasani.paralleltranscoding.linux

import com.gurdasani.paralleltranscoding.linux.LinuxHeterogeneousCorePartitioner.applyToSysfsCpus

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path}
import scala.annotation.tailrec

object LinuxLogicalProcessorPartitioner {
  private val TOPOLOGY_SUBDIR = "topology"
  private val THREAD_SIBLINGS_FILE = "thread_siblings"

  case class LogicalProcessors(ids: Seq[Int])

  def partitionLogicalProcessors(): Seq[LogicalProcessors] =
    applyToSysfsCpus { path =>
      LogicalProcessors(idMaskToIds(findThreadSiblingsMask(path), Seq(), 0))
    }.distinct

  @tailrec
  private def idMaskToIds(mask: Int, idsSoFar: Seq[Int], thisId: Int): Seq[Int] = mask match {
    case 0 => idsSoFar.reverse
    case mask if (mask & 1) == 1 => idMaskToIds(mask >> 1, thisId +: idsSoFar, thisId + 1)
    case _ => idMaskToIds(mask >> 1, idsSoFar, thisId + 1)
  }
  private def findThreadSiblingsMask(path: Path): Int = readHexIntFromFile(
    path.resolve(TOPOLOGY_SUBDIR).resolve(THREAD_SIBLINGS_FILE))
  private def readHexIntFromFile(path: Path): Int =
    Integer.parseInt(Files.readString(path, StandardCharsets.UTF_8).trim, 16)
}
