package com.gurdasani.paralleltranscoding.linux

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, Paths}
import scala.compat.java8.StreamConverters._

object LinuxHeterogeneousCorePartitioner {
  private val CPU_SYSFS_PARENT = Paths.get("/sys", "devices", "system", "cpu")
  private val LOGICAL_PROCESSOR_PATTERN = """cpu\d+""".r
  private val CPUFREQ_SUBDIR = "cpufreq"
  private val MAX_FREQ_FILE = "cpuinfo_max_freq"
  private val CORE_ID_FILE = "related_cpus"

  case class HeterogeneousCores(performanceCores: Seq[Int], efficiencyCores: Seq[Int])
  private case class CoreFrequency(coreId: Int, frequencyMHz: Int)

  def partitionCores(): HeterogeneousCores = {
    val coreFrequencies: Seq[CoreFrequency] = findCoreFrequencies()
    val frequencyGroups = coreFrequencies.groupBy(_.frequencyMHz)
    val lowestFrequency = frequencyGroups.keys.min
    val efficiencyCores = for (freq <- frequencyGroups(lowestFrequency)) yield freq.coreId
    val performanceCores = for (freq <- coreFrequencies if !efficiencyCores.contains(freq.coreId)) yield freq.coreId
    HeterogeneousCores(performanceCores, efficiencyCores)
  }

  private def findCoreFrequencies(): Seq[CoreFrequency] =
    applyToSysfsCpus(path => CoreFrequency(findCoreId(path), findMaxFrequency(path)))

  private[linux] def applyToSysfsCpus[T](mapper: Path => T): Seq[T] = for (
    path <- Files.walk(CPU_SYSFS_PARENT).toScala(Seq)
    if LOGICAL_PROCESSOR_PATTERN.matches(path.getFileName.toString)
  ) yield mapper.apply(path)
  private def findCoreId(path: Path): Int = readIntFromFile(path.resolve(CPUFREQ_SUBDIR).resolve(CORE_ID_FILE))
  private def findMaxFrequency(path: Path): Int = readIntFromFile(path.resolve(CPUFREQ_SUBDIR).resolve(MAX_FREQ_FILE))
  private def readIntFromFile(path: Path): Int = Files.readString(path, StandardCharsets.UTF_8).trim.toInt
}
