package com.gurdasani.paralleltranscoding

import com.amazonaws.services.sqs.model.{Message, ReceiveMessageRequest}
import org.slf4j.LoggerFactory

import java.util.concurrent.{RejectedExecutionHandler, SynchronousQueue, ThreadPoolExecutor, TimeUnit}
import scala.jdk.CollectionConverters._

object ShunterMain {
  private[this] val logger = LoggerFactory.getLogger(getClass)
  def main(args: Array[String]): Unit = {
    val mainQueue = "parallel-transcode"
    val dlQueue = "parallel-transcode-dlq"
    val profile = "poweruser"
    val mainSqs = new SQSConfigurer(mainQueue, profile)
    val dlqSqs = new SQSConfigurer(dlQueue, profile)

    val threadPool = new ThreadPoolExecutor(32, 32, Long.MaxValue, TimeUnit.DAYS, new SynchronousQueue[Runnable],
      new RejectedExecutionHandler {
        override def rejectedExecution(runnable: Runnable, threadPoolExecutor: ThreadPoolExecutor): Unit
          = threadPoolExecutor.getQueue.put(runnable)
      })

    val receiveRequest = new ReceiveMessageRequest()
        .withMaxNumberOfMessages(10)
        .withVisibilityTimeout(120)
        .withWaitTimeSeconds(20)
        .withQueueUrl(dlqSqs.sqsUrl)

    for (task <- LazyList.continually(new Runnable {
      def run(): Unit = {
        for (message: Message <- dlqSqs.client.receiveMessage(receiveRequest).getMessages.asScala) {
          try {
            threadPool.submit(new Runnable {
              def run(): Unit = {
                mainSqs.client.sendMessage(mainSqs.sqsUrl, message.getBody)
                logger.debug("Sent message {}", message.getBody)
                dlqSqs.client.deleteMessage(dlqSqs.sqsUrl, message.getReceiptHandle)
              }
            })
          } catch {
            case e: Exception =>
              logger.error("Shunt failed, not deleting message {}: {}",
                message.getReceiptHandle, e.getMessage, e)
          }
        }
      }
    })) {
      threadPool.submit(task)
    }
  }
}
