package com.gurdasani.paralleltranscoding

import java.io.File

object HostType {
  case class Host(num: Int, prefix: File)
}
