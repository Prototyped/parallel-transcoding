package com.gurdasani.paralleltranscoding

trait CommandPrefixGenerator {
  def generateCommandPrefix(): Seq[String]
}
