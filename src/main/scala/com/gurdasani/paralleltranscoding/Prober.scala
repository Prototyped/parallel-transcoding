package com.gurdasani.paralleltranscoding

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.gurdasani.paralleltranscoding.Prober.{AudioStream, JSONStream, Subtitle}
import org.apache.commons.exec.{CommandLine, DefaultExecutor, ExecuteException, PumpStreamHandler}
import org.slf4j.LoggerFactory

import java.io.{ByteArrayOutputStream, InputStream, InputStreamReader}
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.util.concurrent.atomic.AtomicReference

private[paralleltranscoding] class Prober(ffprobe: Path) {
  import Prober.{JSONStreams, Media, MediaStream, VideoStream}

  private val logger = LoggerFactory.getLogger(getClass)
  private val exec = new DefaultExecutor
  private val mapper = JsonMapper.builder()
    .addModule(DefaultScalaModule)
    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    .build()

  private[paralleltranscoding] def probe(media: Path): Media = {
    val commandLine = new CommandLine(ffprobe.toFile).addArguments(args(media), false)
    val result = new AtomicReference[JSONStreams]
    val err = new ByteArrayOutputStream
    exec.setStreamHandler(new PumpStreamHandler(null, err) {
      override def setProcessOutputStream(is: InputStream): Unit = {
        result.set(mapper.readValue(new InputStreamReader(is, StandardCharsets.UTF_8), classOf[JSONStreams]))
      }
    })
    try {
      exec.execute(commandLine)
    } catch {
      case e: ExecuteException =>
        throw new IllegalStateException(
          "ffprobe execution failed with exit code " + e.getExitValue + ":\n" + err.toString(StandardCharsets.UTF_8))
    }

    mapResult(result.get)
  }

  private def mapResult(streams: JSONStreams): Media = {
    val mediaStreams = streams.streams flatMap { jsonStream =>
      jsonStream.codec_type match {
        case "video" => mapVideoStream(jsonStream)
        case "audio" => mapAudioStream(jsonStream)
        case "subtitle" => mapSubtitle(jsonStream)
        case _ => Nil
      }
    }

    val videoStreams = mediaStreams.filter(_.isInstanceOf[VideoStream]).asInstanceOf[List[VideoStream]]
    val audioStreams = mediaStreams.filter(_.isInstanceOf[AudioStream]).asInstanceOf[List[AudioStream]]
    val subtitles = mediaStreams.filter(_.isInstanceOf[Subtitle]).asInstanceOf[List[Subtitle]]

    Media(
      videoStreams.head,
      audioStreams.filter { stream =>
        stream.language match {
          case Some("eng") | Some("english") | Some("en") | Some("Eng") | Some("English") | Some("En") | None => true
          case _ => false
        }
      }.take(1).appended(audioStreams.head).head,
      subtitles.filter(_.title.getOrElse("").isEmpty).take(1)
        .appendedAll(subtitles.filter(_.title.getOrElse("") == "SDH").take(1))
        .headOption)
  }

  private def mapVideoStream(stream: JSONStream): List[MediaStream] = List(VideoStream(
    index = stream.index,
    codec = stream.codec_name,
    profile = stream.profile.getOrElse("Main"),
    width = stream.coded_width.get,
    height = stream.coded_height.get,
    aspectRatio = stream.display_aspect_ratio.getOrElse(
      (stream.coded_width.get.toDouble / stream.coded_height.get).toString),
    pixelFormat = stream.pix_fmt.get,
    frameRate = stream.r_frame_rate.get))

  private def mapAudioStream(stream: JSONStream): List[MediaStream] = List(AudioStream(
    index = stream.index,
    codec = stream.codec_name,
    profile = stream.profile,
    channels = stream.channels.get,
    channelLayout = if (stream.channel_layout.isDefined) stream.channel_layout.get
    else if (stream.channels.get == 1) "mono"
    else if (stream.channels.get == 2) "stereo"
    else if (stream.channels.get == 6) "5.1"
    else "something else",
    sampleRate = Integer.parseInt(stream.sample_rate.get),
    language = if (stream.tags != null) stream.tags.language else None))

  private def mapSubtitle(stream: JSONStream): List[MediaStream] = {
    if (stream.tags == null) Nil
    else stream.tags.language match {
      case Some("eng") | Some("english") | Some("en") | Some("Eng") | Some("English") | Some("En") | None =>
        List(Subtitle(index = stream.index, codec = stream.codec_name, language = stream.tags.language,
          title = stream.tags.title))
      case Some(_) => Nil
    }
  }
  private def args(media: Path): Array[String] = Array(
    "-loglevel", "fatal", "-show_streams", "-print_format", "json", media.toString)
}

private[paralleltranscoding] object Prober {
  private[paralleltranscoding] case class Media(
    videoStream: VideoStream,
    audioStream: AudioStream,
    subtitle: Option[Subtitle])

  private[paralleltranscoding] trait MediaStream
  private[paralleltranscoding] case class VideoStream(
    index: Int,
    codec: String,
    profile: String,
    width: Int,
    height: Int,
    aspectRatio: String,
    pixelFormat: String,
    frameRate: String) extends MediaStream
  private[paralleltranscoding] case class AudioStream(
    index: Int,
    codec: String,
    profile: Option[String],
    channels: Int,
    channelLayout: String,
    sampleRate: Int, language: Option[String]) extends MediaStream
  private[paralleltranscoding] case class Subtitle(
    index: Int,
    codec: String,
    title: Option[String],
    language: Option[String]) extends MediaStream

  private[paralleltranscoding] case class JSONStreams(streams: List[JSONStream])
  private[paralleltranscoding] case class JSONStream(
    index: Int,
    codec_name: String,
    profile: Option[String],
    codec_type: String,
    coded_width: Option[Int],
    coded_height: Option[Int],
    display_aspect_ratio: Option[String],
    pix_fmt: Option[String],
    r_frame_rate: Option[String],
    channels: Option[Int],
    channel_layout: Option[String],
    sample_rate: Option[String],
    tags: JSONTags)

  private[paralleltranscoding] case class JSONTags(language: Option[String], title: Option[String])
}