package com.gurdasani.paralleltranscoding

import com.amazonaws.services.sqs.model.{ChangeMessageVisibilityRequest, Message}
import com.gurdasani.paralleltranscoding.ConsumerMain.{Encoding, HardwareHEVC, HardwareTranscode, Software, VideoToolbox}
import com.gurdasani.paralleltranscoding.HostType.Host
import com.gurdasani.paralleltranscoding.Prober.Media
import com.gurdasani.paralleltranscoding.linux.LinuxAllowedCpusCommandPrefixGenerator
import org.apache.commons.exec.{CommandLine, DefaultExecutor}
import org.slf4j.LoggerFactory

import java.io.File
import java.nio.file.Paths
import java.util.concurrent.{Executors, TimeUnit}
import scala.concurrent.duration.DurationInt

class Transcoder(ffmpeg: File, ffprobe: File, encoding: Encoding, hw: HardwareTranscode, host: Host, sqs: SQSConfigurer,
                 avx512: Boolean, heterogeneousCoreType: Option[HeterogeneousCoreType],
                 logicalProcessorOrdinal: Option[Int]) {
  private val logger = LoggerFactory.getLogger(getClass)
  private val prober = new Prober(ffprobe.toPath)
  private val exec = new DefaultExecutor

  def transcode(msg: Message): Unit = {
    val executor = Executors.newScheduledThreadPool(1)
    try {
      executor.scheduleAtFixedRate(() => extendVisibility(msg.getReceiptHandle), 5, 5, TimeUnit.MINUTES)
      val loc = msg.getBody
      logger.info("transcoding {}", loc)

      execute(loc)

      sqs.client.deleteMessage(sqs.sqsUrl, msg.getReceiptHandle)
    } finally {
      executor.shutdown()
    }
  }

  private def extendVisibility(handle: String): Unit = {
    val request = new ChangeMessageVisibilityRequest(sqs.sqsUrl, handle, 10.minutes.toSeconds.asInstanceOf[Int])
    sqs.client.changeMessageVisibility(request)
  }

  private def execute(loc: String): Unit = {
    try {
      val fullLoc = withPrefix(loc)
      val media = prober.probe(Paths.get(fullLoc))
      val bits = bitness(media)
      val hostNum = host match {
        case Host(num, _) => num
      }
      val target = fullLoc + "." + hostNum + encoding.suffix
      val videoMapArgs = Array("-map", "0:" + media.videoStream.index)
      val inputArgs = inputArguments(media)
      val videoArgs = videoFilters(media)
      val audioArgs = audioEncodingArguments(media)
      val subtitleArgs = subtitleEncodingArguments(media)
      val commandLinePrefix = if (heterogeneousCoreType.isDefined || logicalProcessorOrdinal.isDefined) {
        val prefixGenerator: CommandPrefixGenerator =
          new LinuxAllowedCpusCommandPrefixGenerator(heterogeneousCoreType, logicalProcessorOrdinal)
        val prefix = prefixGenerator.generateCommandPrefix()
        logger.info("Command line prefix for heterogeneous core type {}, logical processor ordinal {}: {}",
          heterogeneousCoreType, logicalProcessorOrdinal, prefix)
        new CommandLine(new File(prefix.head))
          .addArguments(prefix.tail.toArray, false)
          .addArgument(ffmpeg.toString, false)
      } else {
        new CommandLine(ffmpeg)
      }
      logger.info("Prefix: {}", commandLinePrefix)

      encoding.videoEncodingArgs(fullLoc, bits, hw, avx512)
        .zipWithIndex
        .foreach {
          case (args, i) =>
            val commandLine = new CommandLine(commandLinePrefix)
              .addArguments(inputArgs, false)
              .addArgument(fullLoc, false)
              .addArguments(videoMapArgs, false)
              .addArguments(args, false)
              .addArguments(videoArgs, false)
              .addArguments(audioArgs, false)
              .addArguments(subtitleArgs, false)
              .addArgument(target, false)
            logger.info("Pass {}: {}", i, commandLine)
            exec.execute(commandLine)
        }
    } catch {
      case e: Exception =>
        logger.error("Fatal error! {}", e.getMessage, e)
        throw e
    }
  }

  private def withPrefix(loc: String) = host match {
    case Host(_, prefix) => prefix.getPath + File.separator + loc
  }

  private def bitness(media: Media) = media.videoStream.profile match {
    case profile: String if profile.endsWith("10") => 10
    case profile: String if profile.endsWith("12") => 12
    case _ => 8
  }

  private def inputArguments(media: Media): Array[String] = {
    List(
      Array("-y"),
      hw.hardwareDecodeArgs(media.videoStream.codec),
      Array("-i")
    ).flatten.toArray[String]
  }

  private def audioEncodingArguments(media: Media): Array[String] = {
    val language = media.audioStream.language.getOrElse("eng")
    val commonArgs = Array("-map", "0:" + media.audioStream.index, "-c:a", "libopus", "-vbr", "on",
      "-metadata:s:a:0", "language=" + language, "-disposition:a:0", "default")
    val channelArgs = media.audioStream.channels match {
      case 1 => Array("-b:a", "96k")
      case 2 => Array("-b:a", "128k")
      case 6 => media.audioStream.channelLayout match {
        case layout: String if layout.contains("side") =>
          Array("-af", "aformat=channel_layouts=5.1", "-b:a", "192k")
        case _ => Array("-b:a", "192k")
      }
      case 8 => media.audioStream.channelLayout match {
        case layout: String if layout.contains("side") =>
          Array("-af", "aformat=channel_layouts=7.1", "-b:a", "224k")
        case _ => Array("-b:a", "224k")
      }
      case _ => throw new IllegalArgumentException(
        "Unknown channel layout " + media.audioStream.channelLayout + " of " + media.audioStream.channels + " channels")
    }

    commonArgs.appendedAll(channelArgs)
  }

  private def subtitleEncodingArguments(media: Media): Array[String] = media.subtitle match {
    case Some(subtitle) =>
      val language = subtitle.language.getOrElse("eng")
      val codec = subtitle.codec match {
        case "mov_text" => "ass"
        case "dvbsub" => "dvdsub"
        case "pgssub" => "dvdsub"
        case "cc_dec" => "ass"
        case "webvtt" => "ass"
        case _ => "copy"
      }
      Array("-map", "0:" + subtitle.index, "-c:s", codec, "-metadata:s:s:0", "language=" + language,
        "-disposition:s:0", "default")
    case None => Array()
  }

  private def videoFilters(media: Media): Array[String] = {
    val hwDownload = hwDownloadFilters(media)
    val scale = scaleFilters(media)
    val hwUpload = hwUploadFilters(media)

    if (!hwDownload.isEmpty || !scale.isEmpty || !hwUpload.isEmpty) {
      if (hw != Software)
        Array("-vf", String.join(",", (scale appendedAll hwDownload) appendedAll hwUpload: _*))
      else if (encoding == HardwareHEVC && hw != VideoToolbox)
        Array("-vf", String.join(",", (hwDownload appendedAll hwUpload) appendedAll scale: _*))
      else
        Array("-vf", String.join(",", (hwDownload appendedAll scale) appendedAll hwUpload: _*))
    } else {
      Array()
    }
  }

  private def hwDownloadFilters(media: Media): Array[String] = if (hw != Software) {
    encoding match {
      case HardwareHEVC => Array()
      case _ => forcedHwDownloadFilters(media)
    }
  } else {
    Array()
  }

  private def forcedHwDownloadFilters(media: Media): Array[String] = Array("hwdownload", "format=" + hwFormat(media))

  private def hwUploadFilters(media: Media): Array[String] = if (hw != Software) {
    Array()
  } else {
    encoding match {
      case HardwareHEVC => Array("format=" + hwFormat(media), "hwupload=extra_hw_frames=40")
      case _ => Array()
    }
  }

  private def scaleFilters(media: Media): Array[String] = {
    val pixels = media.videoStream.width * media.videoStream.height
    val scaleFactor = Math.sqrt(921600.0 / pixels)
    if (scaleFactor < 0.8) {
      val (encodingWidth: Int, encodingHeight: Int) =
        (Math.round(media.videoStream.width * scaleFactor + 8).toInt & ~0xF,
          Math.round(media.videoStream.height * scaleFactor + 8).toInt & ~0xF)
      if (hw != VideoToolbox) hw.scaleArgs(encodingWidth, encodingHeight)
      else forcedHwDownloadFilters(media) ++ hw.scaleArgs(encodingWidth, encodingHeight)
    } else Array()
  }

  private def hwFormat(media: Media): String = media.videoStream.profile match {
    case profile: String if profile.endsWith("10") => "p010"
    case _ => "nv12"
  }
}
