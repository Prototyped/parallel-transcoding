package com.gurdasani.paralleltranscoding

import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.model.Message
import com.gurdasani.paralleltranscoding.ConsumerMain.{HEVC, HardwareHEVC, QuickSync, Software}
import com.gurdasani.paralleltranscoding.HostType.Host
import org.mockito.BDDMockito.`given`
import org.mockito.Mockito.mock
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import java.nio.file.{Path, Paths}

class TranscoderITCase extends AnyFlatSpec with should.Matchers with BeforeAndAfterEach {
  private val prefix = Paths.get("/srv", "data", "home", "tv", "finished", "transcode")
  private val ffprobe = Paths.get("/usr", "bin", "ffprobe")
  private val ffmpeg = Paths.get("/usr", "bin", "ffmpeg")
  private val prober = new Prober(ffprobe)
  private var toDelete: List[Path] = List()

  "Transcoder" should "hardware transcode a HEVC 10 bit 1080p video with 5.1 channel audio with appropriate parameters" in {
    val file = "Seal Team S01E01 Tip of the Spear  (1080p x265 10bit Joy).mkv"
    val sqsConfigurer = mock(classOf[SQSConfigurer])
    given(sqsConfigurer.client).willReturn(mock(classOf[AmazonSQS]))
    given(sqsConfigurer.sqsUrl).willReturn("https://somequeue/somewhere")
    val transcoder = new Transcoder(ffmpeg.toFile, ffprobe.toFile, HardwareHEVC, Software, Host(0, prefix.toFile), sqsConfigurer, false, None)
    val expectedPath = prefix.resolve(file + ".0.mkv")
    toDelete = List(expectedPath)

    transcoder.transcode(new Message().withBody(file))

    expectedPath.toFile should exist
    val media = prober.probe(expectedPath)
    media.videoStream should have (
      Symbol("codec") ("hevc"),
      Symbol("profile") ("Main 10"),
      Symbol("width") (1280),
      Symbol("height") (720),
      Symbol("aspectRatio") ("16:9"),
      Symbol("frameRate") ("24000/1001"))
    media.audioStream should have (
      Symbol("codec") ("opus"),
      Symbol("channels") (6),
      Symbol("channelLayout") ("5.1"),
      Symbol("sampleRate") (48000),
      Symbol("language") (Some("eng")))
    media.subtitle should have (
      Symbol("codec") ("subrip"),
      Symbol("language") (Some("eng")))
  }

  it should "software transcode a H.264 8 bit 720p video with stereo audio with appropriate parameters" in {
    val prefix = Paths.get("/srv", "data", "home", "tv", "finished", "transcode")
    val file = "Black.Monday.S01E01.720p.AMZN.WEBRip.x264-GalaxyTV.mkv"
    val ffprobe = Paths.get("/usr", "bin", "ffprobe")
    val ffmpeg = Paths.get("/usr", "bin", "ffmpeg")
    val sqsConfigurer = mock(classOf[SQSConfigurer])
    given(sqsConfigurer.client).willReturn(mock(classOf[AmazonSQS]))
    given(sqsConfigurer.sqsUrl).willReturn("https://somequeue/somewhere")
    val transcoder = new Transcoder(ffmpeg.toFile, ffprobe.toFile, HEVC, Software, Host(0, prefix.toFile), sqsConfigurer, false, None)
    val expectedPath = prefix.resolve(file + ".0.mkv")
    toDelete = List(expectedPath)

    transcoder.transcode(new Message().withBody(file))

    expectedPath.toFile should exist
    val media = prober.probe(expectedPath)
    media.videoStream should have (
      Symbol("codec") ("hevc"),
      Symbol("profile") ("Main"),
      Symbol("width") (1280),
      Symbol("height") (720),
      Symbol("aspectRatio") ("16:9"),
      Symbol("frameRate") ("24000/1001"))
    media.audioStream should have (
      Symbol("codec") ("opus"),
      Symbol("channels") (2),
      Symbol("channelLayout") ("stereo"),
      Symbol("sampleRate") (48000),
      Symbol("language") (Some("eng")))
    media.subtitle should have (
      Symbol("codec") ("subrip"),
      Symbol("language") (Some("eng")))
  }
  override protected def afterEach(): Unit = {
    toDelete.foreach(java.nio.file.Files.deleteIfExists)
    toDelete = List()
  }
}
