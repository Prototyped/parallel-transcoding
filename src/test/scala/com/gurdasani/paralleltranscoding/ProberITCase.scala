package com.gurdasani.paralleltranscoding

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import java.nio.file.Paths

class ProberITCase extends AnyFlatSpec with should.Matchers {
  "Prober" should "probe a file with video, English audio and English subtitles" in {
    val prober = new Prober(Paths.get("/usr", "bin", "ffprobe"))
    val result = prober.probe(Paths.get("/srv", "data", "home", "tv", "finished", "transcode",
      "Seal Team S01E01 Tip of the Spear  (1080p x265 10bit Joy).mkv"))

    result.videoStream should have (
      Symbol("index") (0),
      Symbol("codec") ("hevc"),
      Symbol("profile") ("Main 10"),
      Symbol("width") (1920),
      Symbol("height") (1080),
      Symbol("aspectRatio") ("16:9"),
      Symbol("pixelFormat") ("yuv420p10le"),
      Symbol("frameRate") ("24000/1001"))

    result.audioStream should have (
      Symbol("index") (1),
      Symbol("codec") ("aac"),
      Symbol("profile") (Some("HE-AAC")),
      Symbol("sampleRate") (48000),
      Symbol("channels") (6),
      Symbol("channelLayout") ("5.1"),
      Symbol("language") (Some("eng")))

    result.subtitle.get should have (
      Symbol("index") (2),
      Symbol("codec") ("subrip"),
      Symbol("language") (Some("eng")))
  }
}
